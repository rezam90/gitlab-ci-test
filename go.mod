module gitlab.com/rezam90/gitlab-ci-test

go 1.14

require (
	github.com/cockroachdb/cockroach-go v2.0.1+incompatible // indirect
	github.com/gobuffalo/attrs v1.0.0 // indirect
	github.com/gobuffalo/envy v1.9.0 // indirect
	github.com/gobuffalo/fizz v1.11.0 // indirect
	github.com/gobuffalo/genny v0.6.0 // indirect
	github.com/gobuffalo/nulls v0.4.0 // indirect
	github.com/gobuffalo/packr/v2 v2.8.0 // indirect
	github.com/gobuffalo/pop v4.13.1+incompatible // indirect
	github.com/gobuffalo/validate v2.0.4+incompatible // indirect
	github.com/gofiber/fiber v1.13.3 // indirect
	github.com/gofrs/uuid v3.3.0+incompatible // indirect
	github.com/lib/pq v1.8.0 // indirect
	github.com/spf13/cobra v1.0.0 // indirect
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208 // indirect
)
