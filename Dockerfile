FROM alpine

RUN apk add --no-cache ca-certificates tzdata

WORKDIR /app

COPY bin/app .
COPY bin/soda .

EXPOSE 4000

CMD ["/app/app"]