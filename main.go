package main

import (
	"log"

	"github.com/gobuffalo/envy"
	"github.com/gofiber/fiber"
)

func main() {
	r := fiber.New()

	r.Get("/", func(c *fiber.Ctx) {
		c.Send("hello world")
	})

	log.Fatalln(r.Listen(envy.Get("PORT", ":4000")))
}
